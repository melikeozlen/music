﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(g151210035.Startup))]
namespace g151210035
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
